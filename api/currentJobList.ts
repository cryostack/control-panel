import Bull from 'bull'
import express from 'express'

const app = express()

app.get('/', (req, res, next) => {
    // req is the Node.js http request object
    const db = new Bull('cryostack-pipeline')
    console.log('Job status')
    console.log(req)
    db.getJobCounts().then((data) => {
        console.log('hey')
        console.log(data)
        res.send(data)
        res.end()
        next()
    })
    // res is the Node.js http response object
    // next is a function to call to invoke the next middleware
    // Don't forget to call next at the end if your middleware is not an endpoint!
})

module.exports = {
    path: '/api/jobCounts',
    handler: app
}