/*
** TailwindCSS Configuration File
**
** Docs: https://tailwindcss.com/docs/configuration
** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
*/
module.exports = {
  theme: {
    extend: {
      height: {
        log: '500px'
      },
      colors: {
        'leiden': '#000B65',
        'transp-grey': 'rgba(20,20,20,0.6)'
      }
    }
  },
  variants: {},
  plugins: []
}
