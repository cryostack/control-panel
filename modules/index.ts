import { Module } from '@nuxt/types'

const buildModules: Module = function () {
    // Add middleware only with `nuxt dev` or `nuxt start`
    if (this.options.dev || this.options._start) {
      this.addModule('~/modules/io/')
    }
}

export default buildModules