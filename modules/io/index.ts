import http from 'http'
import socketIO from 'socket.io'
import { Module } from '@nuxt/types'
import Bull from 'bull'

import config from '../../nuxt.config';

interface IJobInfo {
    id: Bull.JobId,
    dataId: string,
    image: string,
    ts: number,
    state: string
}

const parseJobData = async(job: Bull.Job): Promise<IJobInfo> => {
    const jobInfo = await job.getState().then((state) => {
        return {
            id: job.id,
            dataId: job.data.dataId,
            image: job.data.image,
            ts: job.timestamp,
            state,
        }
    })
    return jobInfo
}

const bullHook: Module = function () {
    const db = new Bull('cryostack-pipeline', {
        host: '0.0.0.0'
    })
    const server = http.createServer(this.nuxt.renderer.app)
    const io = socketIO(server)
    // overwrite nuxt.server.listen()
    const serverOpts = {
        port: 3000,
        host: 'localhost'
    }
    if (config.server) {
        serverOpts.port = config.server.port as number
        serverOpts.host = config.server.host as string
    } 
    this.nuxt.server.listen = (port, host) => new Promise(resolve => server.listen(serverOpts.port, serverOpts.host, resolve))
    // close this server on 'close' event
    this.nuxt.hook('close', () => async () => {
        await db.close().then(server.close)
    })
    // Add socket.io events
    
    io.on('connection', (socket) => {
        const updateStatus = () => {
            db.getJobCounts().then((data: any) => {
                socket.emit('currentStatus', data)
            }).catch((err) => {
                throw err
            })
        }
        socket.on('getLog', (jobId) => {
            db.getJobLogs(jobId).then((logs: {logs: string[], count: number}) => {
                socket.emit(`jobLog-${jobId}`, logs.logs)
            })
        })
        socket.on('updateStatus', updateStatus)
        socket.on('newView', () => {
            db.getJobs([
                'waiting',
                'active',
                'stalled',
                'completed',
                'failed',
                'paused',
                'removed'
            ]).then(async (data: Bull.Job[]) => {
                const jobList: IJobInfo[] = []
                for (let i = 0; i < data.length; i++) {
                    const job = data[i]
                    jobList.push(await parseJobData(job))
                }
                const sorted = jobList.sort((a, b) => (b.id as number) - (a.id as number))
                socket.emit('current-list', jobList)
            }).catch((err) => {
                throw err
            })
        })
        db.on('global:active', (data) => {
            updateStatus()
            console.log('New job activated')
            db.getJob(data).then(async (job) => {
                console.log(`Got the job that was just activated ${job.id}`)
                if (job) {
                    const jobInfo = await parseJobData(job)
                    socket.emit('active', jobInfo)
                }
            })
        })
        db.on('global:completed', (data) => {
            updateStatus()
            console.log('New job completed')
            db.getJob(data).then(async (job) => {
                console.log(`Got the job that was just completed ${job.id}`)
                if (job) {
                    const jobInfo = await parseJobData(job)
                    socket.emit('completed', jobInfo)
                }
            })
        })
        db.on('global:waiting', (data) => {
            updateStatus()
            console.log('New job waiting')
            db.getJob(data).then(async (job) => {
                if (job) {
                    const jobInfo = await parseJobData(job)
                    socket.emit('waiting', jobInfo)
                }
            })
        })
        db.on('global:failed', (data, err) => {
            console.log('New job failed')
            updateStatus()
            console.log(err)
            db.getJob(data).then(async (job) => {
                if (job) {
                    const jobInfo = await parseJobData(job)
                    socket.emit('failed', { jobInfo, err })
                }
            })
        })
    })
}

export default bullHook


