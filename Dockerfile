FROM node:10.16.0-alpine
LABEL mantainer Davi Ortega <davi.ortega@cryostack.org>

RUN apk update && apk upgrade

ADD package.json /tmp/package.json
RUN cd /tmp && npm install
RUN mkdir -p /opt/app && cp -a /tmp/node_modules /opt/app/

WORKDIR /opt/app
ADD . /opt/app

EXPOSE 5000

RUN npm run build
CMD npm start